<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('producto_id');
            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('tipo_id')->on('tipos');
            $table->integer('marca_id')->unsigned();
            $table->foreign('marca_id')->references('marca_id')->on('marcas');
            $table->string('nombre', 40)->nullable();
            $table->string('descripcion', 40)->nullable();
            $table->timestamps();
            $table->unique('producto_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}