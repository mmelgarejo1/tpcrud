<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductoController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('producto/list', [ProductoController::class, 'list'])->name('producto.list');

Route::get('producto/create', [ProductoController::class, 'create'])->name('producto.create');

Route::post('producto/create', [ProductoController::class, 'store'])->name('producto.store');

Route::get('producto/{producto}', [ProductoController::class, 'edit'])->name('producto.edit');

Route::put('producto/{producto}', [ProductoController::class, 'update'])->name('producto.update');

Route::delete('producto/{producto}',[ProductoController::class, 'destroy'])->name('producto.destroy');
