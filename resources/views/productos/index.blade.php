@extends('layouts.app')

@section('title', 'Lista de Productos')

@section('content')
    <div class='container text-center'>
        <h4 class='text-center'> Listado de Productos </h4>
        <div class='row d-flex justify-content-center'>
            <div class='col-md-8'>
                <table class='table text-center'>
                    <tr>
                        <th> ID </th>
                        <th> Nombre </th>
                        <th> Descripcion </th>
                        <th> Marca </th>
                        <th> Tipo </th>
                        <th class="text-center" colspan="2"> Acciones <th>
                    </tr>

                    @foreach($productos as $producto)
                        <tr>
                            <td> {{$producto->producto_id}} </td>
                            <td> {{$producto->nombreproducto}} </td>
                            <td> {{$producto->descripcion}} </td>
                            <td> {{$producto->nombremarca}} </td>
                            <td> {{$producto->nombretipo}} </td>
                            <form action="{{route('producto.destroy', $producto)}}" method="POST">
                                @csrf
                                @method('delete')
                                <td><button class='btn btn-danger'>Eliminar</button></td>
                            </form>
                            <td><a href={{route('producto.edit', $producto->producto_id)}} class='btn btn-info'>Editar</a></td>
                        </tr>
                    @endforeach
                </table>
                <a href={{route('producto.create')}} class='btn btn-primary'>Insertar Producto</a>
            </div>
        </div>
    </div>
@endsection