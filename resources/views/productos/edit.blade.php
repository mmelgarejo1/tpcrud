@extends('layouts.app')

@section('title', 'Editar Producto')

@section('content')
    <div class="container-fluid bg-3 text-center">
        <div class="panel panel-primary">
            <div class="panel-heading">Editar Producto</div>
            <br>
            <form class="{{route('producto.update', $producto)}}" method="POST">
                @csrf
                @method('put')
                <div class="panel-body">
                    <div class="row justify-content-center form-group">
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="product_name" value="{{$producto->nombre}}" placeholder="Nombre del Producto" autofocus required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center form-group">
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="description" value="{{$producto->descripcion}}" placeholder="Descripcion" required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center form-group">
                        <div class="col-md-3">
                            <select class="form-select" aria-label="Default select example" name="marca">
                            @foreach($marcas as $marca)
                                @if($producto->marca_id==$marca->marca_id)
                                    <option value={{$marca->marca_id}} selected>{{$marca->nombre}}</option>
                                @else
                                    <option value={{$marca->marca_id}}>{{$marca->nombre}}</option>
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center form-group">
                        <div class="col-md-3">
                            <select class="form-select" aria-label="Default select example" name="tipo">
                            @foreach($tipos as $tipo)
                                @if($producto->tipo_id==$tipo->tipo_id)
                                    <option value={{$tipo->tipo_id}} selected>{{$tipo->nombre}}</option>
                                @else
                                    <option value={{$tipo->tipo_id}}>{{$tipo->nombre}}</option>
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-success"> Guardar Cambios </button>
                    <a href={{route('producto.list')}} class="btn btn-primary">Volver</a>
                </div>
            </form>
        </div>
    </div>
@endsection

