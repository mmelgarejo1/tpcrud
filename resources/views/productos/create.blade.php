@extends('layouts.app')

@section('title', 'Crear Producto')

@section('content')
    <div class="container-fluid bg-3 text-center">
        <div class="panel panel-primary">
            <div class="panel-heading">Registrar Producto</div>
            <br>
            <form class="form-horizontal" action={{route('producto.store')}} method="post">
                @csrf
                <div class="panel-body">
                    <div class="row justify-content-center form-group">
                        <div class='col-md-3'>
                            <input class="form-control" type="text" name="product_name" placeholder="Nombre del Producto" autofocus required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center form-group">
                        <div class='col-md-3'>
                            <input class="form-control" type="text" name="description" placeholder="Descripcion" required>
                        </div>
                    </div>
                    <br>
                    <div class='row justify-content-center form-group'>
                        <div class='col-md-3'>
                            <select class='form-select' aria-label='Default select example' name='marca'>
                                @foreach($marcas as $marca)
                                    <option value={{$marca->marca_id}}> {{$marca->nombre}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class='row justify-content-center form-group'>
                        <div class='col-md-3'>
                            <select class='form-select' aria-label='Default select example' name='tipo'>
                                @foreach($tipos as $tipo)
                                    <option value={{$tipo->tipo_id}}> {{$tipo->nombre}} </option> 
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-success"> Guardar </button>
                    <a href={{route('producto.list')}} class="btn btn-primary">Volver</a>
                </div>
            </form>
        </div>
    </div>  
@endsection