<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Marca;
use App\Models\Tipo;

class ProductoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
    }

    public function create() {
        $marcas = Marca::all();
        $tipos = Tipo::all();

        return view('productos.create', compact('marcas', 'tipos'));
    }

    public function store(Request $request) {
        $producto = new Producto();
        $producto->nombre = $request->product_name;
        $producto->descripcion = $request->description;
        $producto->marca_id = $request->marca;
        $producto->tipo_id = $request->tipo;
        
        $producto->save();

        return redirect()->route('producto.list');
    }   

    public function edit(Producto $producto) {
        $marcas = Marca::all();
        $tipos = Tipo::all();
        return view('productos.edit', compact('producto', 'marcas', 'tipos'));
    }

    public function list() {
        $productos = Producto::join('marcas', 'productos.marca_id', '=', 'marcas.marca_id')
            ->join('tipos', 'productos.tipo_id', '=', 'tipos.tipo_id')
            ->select('productos.producto_id', 'productos.nombre as nombreproducto', 'productos.descripcion', 'marcas.nombre as nombremarca', 'tipos.nombre as nombretipo')
            ->orderBy('nombreproducto')
            ->get();
        return view('productos.index', compact('productos'));
    }

    public function update(Request $request, Producto $producto) {
        $producto->nombre = $request->product_name;
        $producto->descripcion = $request->description;
        $producto->marca_id = $request->marca;
        $producto->tipo_id = $request->tipo;

        $producto->save();

        return redirect()->route('producto.list');
    }

    public function destroy(Producto $producto) {
        $producto->delete();

        return redirect()->route('producto.list');
    }
}
